import math

class Arabigo():
    
    def __init__(self):

        #Tenemos un diccionario que asocia los números romanos con su valor en número arábigo.
        self.listaNumeros = {'M':1000, 'D':500,'C':100,'L':50,'X':10,'V':5,'I':1}  
    
    def arab(self, numero):
    
        suma = 0
        
        
        if self.validarRomano(numero) == True: #Es método que se encuentra más abajo, simplemente comprueba que el número
                                                # romano que introducimos para convertir en arábigo, es una cadena, 'str'.
            
            suma = 0
            indice = 0
            
            while indice < len(numero): 
                
                if indice == len(numero)-1: #Este 'if' dice: la útima cifra de mi número romano, siempre se va a sumar.
                    
                    suma += self.listaNumeros.get(numero[indice]) #Voy al diccionario, y asocio mi cifra romana (I,V,X...) con su valor en arábigo. 
                                                                   #y meto el valor en arábigo en la variable suma.
            
                elif self.listaNumeros.get(numero[indice]) < self.listaNumeros.get(numero[indice+1]): #Este 'elif' dice: si mi cifra romana es menor que la siguiente cifra romana,
                                                                                                    # restame el valor equivalente en arábigo de mi cifra romana.
                                                                                                    # es decir: XI --> X = 10 e I = 1. Como X > I , como X es mayor que la siguiente cifra (I), me sumas X.
                                                                                                    # es decir: IX --> I = 1 y X = 10. Es decir, como 'I' es menor que la siguiente cifra 'X'. Me restas el valor 
                                                                                                    # equivalente de 'I' en arábigo, es decir, me restas 1.

                
                    suma -= self.listaNumeros.get(numero[indice])
                
                else:
                    suma += self.listaNumeros.get(numero[indice]) #Si no es la última cifra, y la cifra tiene un valor superior a la cifra 
                                                                    # que le sigue, me sumas la cifra.
                
                
                indice +=1
            
        print(suma)
        
        return suma
            


    def validarRomano(self,numero):
        
        if isinstance(numero,str):
            for letra in numero:
                if letra in self.listaNumeros:
                    return True
        
        else:
            print('Hay un error. Introduzca el numero de nuevo')    


 #Clase en la que se incializan los principales métodos del programa.       
class mainApp():

    def __init__(self):
        self.a = Arabigo()
        self.a.arab('DCCLVII') #AQUÍ SE METE EL NÚMERO ROMANO QUE QUEREMOS CONVERTIR.
  

if __name__ == '__main__':

    juego = mainApp() #Inicializamos el programa.
            
