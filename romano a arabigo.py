import math

class Romano():

    def __init__(self):
        
        #En estas listas la posición de cada número romano, coincide con su valor en número arábigo.
        #Me explico, por ejemplo en la lista unidad, el número romano 'V' está en la posición 5, 
        # el número romano 'VII' está en la posición 7.

        self.unidad = ['','I','II','III','IV','V','VI','VII','VIII','IX'] 
        self.decena = ['','X','XX','XXX','XL','L','LX','LXX','LXXX','XC']
        self.centena = ['','C','CC','CCC','CD','D','DC','DCC','DCCC','CM']
        self.millar = ['','M','MM','MMM']
        self.listaVacia = []
    
    def validarArab(self,numero):
    
        if isinstance(numero,int):
            if numero > 0 and numero <= 3999: #Es una forma de validar que el número que queremos convertir está dentro del rango de conversión posible.
                return True
    
        else:
            print('Hay un error. Introduzca el numero de nuevo')

    def romanos(self,numero):   # Método principal que convierte los números arábigos a romanos.
        
        self.rom = '' #Es una lista vacía donde se van metiendo los distintos números romanos que vamos obteniendo.
                      #Al final, el conjunto de números romanos que haya en esta lista, formarán el número final.
    
        if self.validarArab(numero) == True:  #Este método se encuentra definido más abajo. Simplemente se asegura
                                              # de que el número que queremos convertir es de tipo entero 'int'.
        
            #Una forma de resolver hacer el programa, es descomponer el número arábigo en undidades, decenas, centenas
            # y millas mediante operaciones matemáticas.

            self.u = int(numero%10)                     #El resto de dividir entre 10 el número, nos da las unidades.
            self.d = int(math.floor((numero/10)%10))    #De la misma forma calculamos decenas.
            self.c = int(math.floor(numero/100)%10)     #De la misma forma calculamos centenas.
            self.m = int(math.floor(numero/1000))           #De la misma forma calculamos millas.
    
            #El siguiente paso es comprobar en cuantas partes se divide nuestro número arábigo que queremos convertir.
            # Si el número es menor que 10, solo necesitaremos dividirlo en unidades.Si el número es mayor que 10 y menor que 100,
            # necesitaremos unidades + decenas. Si es mayor que 99 y menor que 1000 necesitaremos centenas + decenas + unidades

            if numero >= 1000:
                self.rom = (self.millar[self.m]+self.centena[self.c]+self.decena[self.d]+self.unidad[self.u])
                
            
            elif numero >= 100:
                self.rom = (self.centena[self.c]+self.decena[self.d]+self.unidad[self.u])
            
            elif numero >= 10:
                self.rom= (self.decena[self.d]+self.unidad[self.u])
            
            else:
                self.rom = (self.unidad[numero])
  
        return self.rom
        
        #Ya tenemos en self.rom el valor de nuestro número romano. Ahora solo falta sacarlo por pantalla.

    def imp(self): # Método para sacar por pantalla el número romano. Se podría haber metido todo junto en el mismo método.
            
        print(self.rom)

#Clase que inicializa los distintos métodos que hacen que funcione el programa.
 
class mainApp():

    def __init__(self):
        self.r = Romano()
        self.r.romanos(3999) #AQUÍ ES DONDE TENEMOS QUE METER EL NÚMERO QUE QUEREMOS CONVERTIR.
        self.r.imp()

if __name__ == '__main__':

    juego = mainApp() #INICIALIZACIÓN DEL PROGRAMA.

